# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


$ \curl -sSL https://get.rvm.io | bash
$ source /home/<user>/.rvm/scripts/rvm
$ rvm -v
$ rvm install ruby
$ ruby -v
$ sudo apt-get install rubygems
$ gem update
$ sudo apt-get install ruby-dev zlib1g-dev liblzma-dev build-essential patch
$ rvm gemset list
$ gem install rails
$ rails -v
